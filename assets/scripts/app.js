angular.module('territoriesApp', ['ui.router', 'ngResource'])
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        url: '/',
        views: {
          'content': {
            templateUrl: 'views/worldwide.html'
          }
        }
      })

      .state('app.apac', {
        url: '/apac',
        views: {
          'content@': {
            templateUrl: 'views/apac.html'
          }
        }
      })
      ;

    $urlRouterProvider.otherwise('/');
  });
