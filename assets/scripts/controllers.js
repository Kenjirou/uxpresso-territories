angular.module('territoriesApp')
.controller('TerritoriesController', ['$scope', 'territoriesFactory', function($scope, territoriesFactory) {
  // logic to output the data from the database
  $scope.territories = territoriesFactory.getTerritory().query(
    function(response) {
      $scope.territories = response;
    }
  );
}])
;
