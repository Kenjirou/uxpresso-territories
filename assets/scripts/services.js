angular.module('territoriesApp')
.constant('baseURL', 'http://localhost:3000/')
.service('territoriesFactory', ['$resource', 'baseURL', function($resource, baseURL) {
  this.getTerritory = function() {
    return $resource(baseURL + 'territories');
  };
}])
;
